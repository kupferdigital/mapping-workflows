images = $(notdir $(wildcard ./images/*))
build-image = docker build --tag $(image):latest ./images/$(image)
build-images:
	$(foreach image, $(images), $(build-image);)

# https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-caching-example
# https://stackoverflow.com/questions/6519234/cant-assign-variable-inside-recipe
define gitlab-ci-build-push-image =
	$(eval base_tag=$(CI_REGISTRY_IMAGE)/$(image))
	docker pull $(base_tag):latest || true
	docker build \
		--cache-from $(base_tag):latest \
		--tag $(base_tag):$(CI_COMMIT_SHA) \
		--tag $(base_tag):latest \
		./images/$(image)
	docker push $(base_tag):$(CI_COMMIT_SHA)
	docker push $(base_tag):latest
endef
gitlab-ci-build-push-images:
	echo $(CI_REGISTRY_PASSWORD) | docker login $(CI_REGISTRY) --username $(CI_REGISTRY_USER) --password-stdin
	$(foreach image, $(images), $(gitlab-ci-build-push-image);)

.PHONY: test
